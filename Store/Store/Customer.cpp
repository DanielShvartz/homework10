#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}
Customer::~Customer() {}

string Customer::GetName() const { return this->_name; }
void Customer::SetName(const string name) { this->_name = name; }

//returns the total sum for payment
double Customer::totalSum() const
{
	//to get all the sum we need to run on the set and get all the items
	double totalS = 0;
	for (set<Item>::iterator iter = _items.begin(); iter != _items.end(); iter++)
		totalS += (*iter).totalPrice(); // we get the iterator, we run from the start until the next of the end
	return totalS; // and we take the item total price
}

//remove item from the set
void Customer::removeItem(Item& item)
{
	set<Item>::iterator iter = _items.find(item); // we find the item and gets it iterator
	if(iter==_items.end()) // if its not found(got in the end)
		std::cout << "Couldn't remove - item doesnt exist\n" << std::endl;
	else
	{
		if ((*iter).GetCount() == 1) // if we have only 1 item
			_items.erase(iter); // we just remove him
		else
		{
			Item newItem = (*iter); // if not we cant access it and change it directly
			_items.erase(newItem); // so we take him and remove him
			newItem.DecCount();; // then we reduce from him
			_items.insert(newItem); // and add him again
		}
	}
}

//add item to the set
void Customer::addItem(Item item)
{
	set<Item>::iterator iter = _items.find(item); // we try to find the item
	if (iter == _items.end()) // if its in the end we dont have any members 
		_items.insert(item); // so we can add
	else // if we have already a item in the set
	{
		Item newItem = (*iter); //  we take the item, item and newitem arent the same
		_items.erase(newItem); // we remove it from the set
		newItem.IncCount(); // add 1 more
		_items.insert(newItem); // insert again
	}
}
void Customer::PrintItems() const
{
	for (set<Item>::iterator iter = _items.begin(); iter != _items.end(); iter++)
		std::cout << "The item name is: " << (*iter).GetName() << " You bought the item: " << (*iter).GetCount() << " times. each one costs: " << (*iter).GetUnitPrice() << endl;
	getchar();
}
