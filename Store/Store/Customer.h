#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string);
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item& item);//remove item from the set
	void PrintItems() const;

	//get and set functions
	string GetName() const;
	void SetName(const string name);

private:
	string _name;
	set<Item> _items;


};
