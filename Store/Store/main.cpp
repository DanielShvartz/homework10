#include"Customer.h"
#include<map>

void printItems(Item itemList[10]);
Customer CreateCustomer(map<string, Customer>& abcCustomers);
void modifyItems(bool wantToBuy, Item itemList[10], Customer& customer);
void addCustomer(map<string, Customer>& abcCustomers, Item items[10]);
void updateCustomer(map<string, Customer>& abcCustomers, Item items[10]);
void printBiggestPrice(map<string, Customer>& abcCustomers);
void PrintMenu();

enum options { BUY = 1, CUSTOMER, PRINT, EXIT };
enum option2 { ADD = 1, REMOVE, BACK };
int main()
{

	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00007", 31.65),
		Item("chicken","00008",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	int choice = 0;
	while (choice != EXIT)
	{
		PrintMenu();
		cin >> choice;
		switch (choice)
		{
		case BUY:
			addCustomer(abcCustomers, itemList);
			break;
		case CUSTOMER:
			updateCustomer(abcCustomers, itemList);
			break;
		case PRINT:
			printBiggestPrice(abcCustomers);
			break;
		default:
			break;
		}
	}

	return 0;
}
void printItems(Item itemList[10])
{
	for (int i = 0; i < 10; i++)
		cout << "Price: " << itemList[i].GetUnitPrice() << " " << itemList[i].getSerialNumber() << " " << itemList[i].GetName() << endl;
}

Customer CreateCustomer(map<string, Customer>& abcCustomers)
{
	string customerName;
	cout << "Hello, Enter your name: ";
	cin >> customerName;

	while (abcCustomers.find(customerName) != abcCustomers.end()) // if he already exists
		cout << "Name already exists"; cin >> customerName;

	Customer customer(customerName); // if not create a customer
	system("cls");
	return customer;
}
void modifyItems(bool wantToBuy, Item itemList[10], Customer& customer)
{
	int choice = -1;
	while (choice != 0 && wantToBuy)
	{
		printItems(itemList);
		cout << "The items you can buy are: (0 to exit)" << endl;
		cout << "What item would you like to buy ? Input: ";
		cin >> choice;
		if (choice > 0 && choice <= 10) //if item exists it is added
		{
			customer.addItem(itemList[choice - 1]);
		}
		else if (choice != 0)
		{
			cout << "Enter diffrent number!" << endl;
			system("pause");
		}
		cout << "Item added." << endl;
	}
	while (choice != 0 && !wantToBuy) //if customer wants to erase an item
	{
		system("cls");
		cout << "Choose item to remove: (0 to exit)" << endl;
		printItems(itemList);
		cin >> choice;
		if (choice > 0 && choice <= 10) //if item exists it might be erased if it is in customers list
		{
			customer.removeItem(itemList[choice - 1]);
		}
		else if (choice != 0)
		{
			cout << "Enter diffrent number!" << endl;
			system("pause");
		}
		cout << "Item removed." << endl;

	}
}


void addCustomer(map<string, Customer>& abcCustomers, Item items[10])
{
	string name = "";
	cout << "Enter your name: ";
	cin >> name;
	while (abcCustomers.find(name) != abcCustomers.end())
	{
		cout << "Enter your name: ";
		cin >> name;
	}
	Customer customer(name); //create new customer
	modifyItems(true, items, customer); //add items to customer list

	abcCustomers.insert(pair<string, Customer>(name, customer));
}

void updateCustomer(map<string, Customer>& abcCustomers, Item items[10])
{
	map<string, Customer>::iterator it;
	string name;
	cout << "Enter your name: "; //get name to search customer
	cin >> name;
	if ((it = abcCustomers.find(name)) != abcCustomers.end()) //if customer exists
	{
		(*it).second.PrintItems();
		int choice = -1;
		while (choice != 3)
		{
			cout << endl;
			cout << "1.Add items\n2.Remove items\n3.Back to menu\n";
			cin >> choice;
			switch (choice)
			{
			case 1: //add items to customer
				modifyItems(true, items, it->second);
				break;
			case 2: //remove items from customer
				modifyItems(false, items, it->second);
				break;
			default:
				break;
			}
		}
	}
	else
	{
		cout << "Cutomer does not exist!!" << endl;
		system("pause");
	}

}
void printBiggestPrice(map<string, Customer>& abcCustomers)
{
	map<string, Customer>::iterator it;
	double max = 0; string name;
	for (it = abcCustomers.begin(); it != abcCustomers.end(); it++)
	{
		if ((*it).second.totalSum() > max)
		{
			max = (*it).second.totalSum();
			name = (*it).first;
		}
	}
	cout << "The person who paid the most is " << name << " and the price is: " << max << endl;
	system("Pause");
}

void PrintMenu()
{
	system("cls");
	cout << "Welcome to MagshiMart!\n" << "1.      to sign as customer and buy items\n"
		<< "2.      to uptade existing customer's items\n" << "3.      to print the customer who pays the most\n"
		"4.      to exit\n" << endl;
}