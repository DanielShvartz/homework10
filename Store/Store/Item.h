#pragma once
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

class Item
{
public:
	Item(string, string, double);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	string GetName() const;
	string getSerialNumber() const;
	int GetCount() const;
	double GetUnitPrice() const;

	void SetName(const string name);
	void setSerialNumber(const string SerialNumber);
	// to set the count we use operator ++ and --
	void DecCount();
	void IncCount();
	void setUnitPrice(const double UnitPrice);
private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};