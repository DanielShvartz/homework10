#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1; //default
}
Item::~Item() { }

string Item::GetName() const { return this->_name; }
string Item::getSerialNumber() const { return this->_serialNumber; }
int Item::GetCount() const { return this->_count; }
double Item::GetUnitPrice() const { return this->_unitPrice; }

void Item::SetName(const string name) { this->_name = name; }
void Item::setSerialNumber(const string SerialNumber) { this->_serialNumber = SerialNumber; }
void Item::setUnitPrice(const double UnitPrice) { this->_unitPrice = UnitPrice; }

void Item::DecCount() { this->_count--; }
void Item::IncCount() { this->_count++; }

//returns _count*_unitPrice
double Item::totalPrice() const { return this->_unitPrice*this->_count; }

//compares the _serialNumber of those items.
bool Item::operator <(const Item& other) const { return this->_serialNumber < other._serialNumber; }
//compares the _serialNumber of those items.
bool Item::operator >(const Item& other) const { return this->_serialNumber > other._serialNumber; }
//compares the _serialNumber of those items.
bool Item::operator ==(const Item& other) const { return this->_serialNumber == other._serialNumber; }